provider "google" {
  project     = "dev-project-314307"
  region      = "us-central1"
  credentials = "mykey.json"
}

resource "google_compute_instance" "testmachine" {
  name         = "testmachine"
  machine_type = "e2-medium"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }


  network_interface {
    network = "default"
  }

}